import "reflect-metadata";
import * as express from "express";
import * as bodyParser from "body-parser";
import * as helmet from "helmet";
import * as cors from "cors";
import routes from "./routes";
import { setUpTestUsers } from "./user/UserRepository";

let whitelist = ['http://localhost:3000']

// Create a new express application instance
const app = express();

// Call midleware
app.use(cors({
    origin: function(origin, callback){
      console.log(origin)

      if(!origin) return callback(null, true);
      if(whitelist.indexOf(origin) === -1){
        let message = 'The CORS policy for this origin does not ' +
                  'allow access from the particular origin.';
        return callback(new Error(message), false);
      }
      return callback(null, true);
    }
  })); 
app.use(helmet());
app.use(bodyParser.json());

//Set all routes from routes folder
app.use("/", routes);

setUpTestUsers();


app.listen(8080, () => {
    console.log("Server started on port 8080!");
});
