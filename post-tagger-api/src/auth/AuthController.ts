import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";

import { User } from "../user/User";
import config from "../config/config";
import { getUserRepository } from "../user/UserRepository";

class AuthController {
  static login = async (req: Request, res: Response) => {
    //Check if username and password are set
    let { username, password } = req.body;
    if (!(username && password)) {
      res.status(400).send();
    }

    //Get user from database
    const userRepository = getUserRepository();
    let user: User;
    try {
      user = await userRepository.fullUserbyName(username);
    } catch (error) {
      res.status(401).send();
      return;
    }

    //Check if encrypted password match
    if (!user.checkIfUnencryptedPasswordIsValid(password)) {
      res.status(401).send();
      return;
    }

    //Sing JWT, valid for 1 hour
    const token = jwt.sign(
      { userId: user.id, username: user.username },
      config.jwtSecret,
      { expiresIn: "1h" }
    );

    //Send the jwt in the response
    res.send(token);
  };
}
export default AuthController;