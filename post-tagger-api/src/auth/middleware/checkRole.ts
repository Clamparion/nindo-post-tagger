import { Request, Response, NextFunction } from "express";


import { User } from "../../user/User";
import { getUserRepository, UserDTO } from "../../user/UserRepository";

export const checkRole = (roles: Array<string>) => {
  return async (req: Request, res: Response, next: NextFunction) => {
    //Get the user ID from previous midleware
    const id = res.locals.jwtPayload.userId;

    //Get user role from the database
    const userRepository = getUserRepository();
    let user: UserDTO;
    try {
      user = await userRepository.byId(id);
    } catch (id) {
      res.status(401).send();
    }

    //Check if array of authorized roles includes the user's role
    if (roles.indexOf(user.role) > -1) next();
    else res.status(401).send();
  };
};