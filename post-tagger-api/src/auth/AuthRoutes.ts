
import { Router } from "express";
import AuthController from "./AuthController";
import { checkJwt } from "./middleware/checkJwt";

const router = Router();
//Login route
router.post("/login", AuthController.login);

export default router;