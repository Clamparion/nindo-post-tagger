
import { Router, Request, Response } from "express";
import auth from "./auth/AuthRoutes";
import user from "./user/UserRoutes";

const routes = Router();

routes.use("/auth", auth);
routes.use("/users", user);

export default routes;