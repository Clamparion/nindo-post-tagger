
import * as bcrypt from "bcryptjs";

export class User {
  private readonly password: string;

  constructor(public readonly id: number,
    public readonly username: string,
    password: string,
    public readonly role: string) {
    this.password = bcrypt.hashSync(password, 8);
  }

  checkIfUnencryptedPasswordIsValid(unencryptedPassword: string) {
    return bcrypt.compareSync(unencryptedPassword, this.password);
  }
}