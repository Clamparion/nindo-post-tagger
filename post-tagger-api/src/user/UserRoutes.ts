import { Router } from "express";
import UserController from "./UserController";
import { checkJwt } from "../auth/middleware/checkJwt";
import { checkRole } from "../auth/middleware/checkRole";

const router = Router();

//Get all users
router.get("/", [checkJwt, checkRole(["ADMIN"])], UserController.listAll);

// Get one user
router.get(
  "/:id([0-9]+)",
  [checkJwt, checkRole(["ADMIN"])],
  UserController.getOneById
);

export default router;