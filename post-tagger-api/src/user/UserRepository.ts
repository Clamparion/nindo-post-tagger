import { User } from "./User";

export type UserDTO = {
    username: string,
    role: string,
}

let users: User[];

export class UserRepository {

    constructor(private readonly users: User[]) {
      
    }

    listAll(): UserDTO[] {
        return this.users.map(user => {return {
            username: user.username,
            role: user.role,
        }}) 
    }

    byId(id: number): UserDTO {
        const foundUser = this.users.find(user => +user.id === +id);
        
        if (foundUser === undefined) {
            throw new Error("User not found!")
        }

        return {
            username: foundUser.username,
            role: foundUser.role,
        } 
    }

    fullUserbyName(username: string): User {
        const foundUser = this.users.find(user => user.username === username);
        if (foundUser === undefined) {
            throw new Error("User not found!")
        }
        return foundUser;
    }

}

export const getUserRepository = (): UserRepository => {
    return new UserRepository(users);
}

export const setUpTestUsers = (): void => {
    users = [
        new User(0, 'admin', 'admin', 'ADMIN'),
        new User(1, 'default', 'default', 'DEFAULT')
    ]
}