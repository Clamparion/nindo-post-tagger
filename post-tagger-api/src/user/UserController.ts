import { Request, Response } from "express";
import { getUserRepository } from "./UserRepository";

class UserController{

static listAll = async (req: Request, res: Response) => {
  const userRepository = getUserRepository();
  res.send(userRepository.listAll());
};

static getOneById = async (req: Request, res: Response) => {
  const id: number = req.params.id;
  const userRepository = getUserRepository();
  try {
    res.send(userRepository.byId(id));
  } catch (error) {
    res.status(404).send("User not found");
  }
};
};

export default UserController;