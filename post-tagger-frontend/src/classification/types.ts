export enum SocialMediaPlatform {
    YouTube = "YouTube",
    Instagram = "Instagram",
    TikTok = "TikTok"
}