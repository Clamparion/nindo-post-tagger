export class Category {
    constructor(
        public readonly id: number,
        public readonly name: string,
        public readonly key: string) { }
}

export function useCategories(): Category[] {
    return [
        new Category(0, "Gaming", "gaming"),
        new Category(1, "Fashion", "fashion"),
        new Category(2, "Beauty", "beauty"),
        new Category(3, "Deko & Einrichtung -> Interior Design", "deko"),
        new Category(4, "Musik", "musik"),
        new Category(5, "Technik", "technik"),
        new Category(6, "Food", "food"),
        new Category(7, "Inhalte für Kinder", "kinder"),
        new Category(8, "Familienleben", "family"),
        new Category(9, "Haustiere", "haustiere"),
        new Category(10, "Sport & Fitness", "fitness"),
        new Category(11, "Autos & Motorräder", "motor"),
        new Category(12, "Fotografie & Art", "fotoart"),
        new Category(13, "Travel", "travel"),
        new Category(14, "Politik", "politik"),
        new Category(15, "Erotik", "erotik"),
    ]
} 