import Meta from "antd/lib/card/Meta";
import { FC } from "react";
import { Checkbox, Row, Col } from 'antd';
import { Category, useCategories } from "../categories";
import { CheckboxValueType } from "antd/lib/checkbox/Group";

type CategorySelectorProps = {
    onSelect?: (categories: Category[]) => void
}

const CategorySelector: FC<CategorySelectorProps> = ({ onSelect }) => {
    const categories = useCategories();


    function onChange(checkedValues: CheckboxValueType[]) {
        const selectedCategories = checkedValues.map(value => categories.find(c => c.id == value)).filter(c => c !== undefined);
        if (onSelect !== undefined && Array.isArray(selectedCategories)) {
            onSelect(selectedCategories as Category[]);
        }
    }

    return (
        <>
            <Checkbox.Group style={{ width: '100%' }} onChange={onChange}>
                <Row>
                    {categories.map(cat => {
                        return (<Col span={24} key={cat.id}>
                            <Checkbox style={{fontSize: '18px', paddingTop: '10px'}} value={cat.id}>{cat.name}</Checkbox>
                        </Col>)
                    })}
                </Row>
            </Checkbox.Group>
        </>
    )
}

export default CategorySelector;

