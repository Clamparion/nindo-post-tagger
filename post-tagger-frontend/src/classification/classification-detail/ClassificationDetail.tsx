
import { Button, Col, Row } from "antd";
import { FC, useState } from "react";
import { Redirect, useParams } from "react-router-dom";
import MainContent from "../../components/MainContent";
import { Category, useCategories } from "../categories";
import YoutubePost from "../posts/YoutubePost";
import { SocialMediaPlatform } from "../types";
import CategorySelector from "./CategorySelector";

type ClassificationDetailProps = {
    platform: SocialMediaPlatform,
    preSelectedCategory: string,
}

const ClassificationDetail: FC = () => {
    const categories = useCategories();
    let { platform, preSelectedCategory } = useParams<ClassificationDetailProps>();

    let [selectedCategories, setSelectedCategories] = useState<Category[]>([]);


    const [
        goBack,
        setGoBack
    ] = useState(false)
    if (goBack === true) {
        return <Redirect to='/classify' />
    }


    const posts = [
        {
            "channelID": "UCCORxOJqKzvpHAXA0pUwglg",
            "postID": "0IY8v5LjAwg",
            "content": "https://i.ytimg.com/vi/0IY8v5LjAwg/maxresdefault.jpg",
            "title": "MAKE UP ROULETTE - PINSEL EDITION \uD83D\uDE33 DAMIT muss ich mich schminken?! mit Alycia Marie",
            "description": "Zum Video von Alycia: https://youtu.be/Xc4Fa1PcmMQ\nZur NEUEN Verlosung: https://www.instagram.com/p/CCdfIgdic4O/\nHier KOSTENLOS abonnieren ❤ : http://bit.ly/2ij4Wis\nInstagram: XLAETA | http://instagram.com/xlaeta\nTikTok: XLAETA | https://www.tiktok.com/@xlaeta\n\nAlycia Marie und meine Wenigkeit haben zusammen eine Runde Make-Up Roulette gespielt. Mit einem ziemlich interessanten Ergebnis!\n\nMein Shop: http://www.xlaeta-shop.de (Werbung)\nMeine eBooks: https://www.supporting.ly/xlaeta (Werbung)\n----------------------------------------------------------------- \nDu findest mich auch auf: \nInstagram: http://instagram.com/xLaeta | XLAETA\nTikTok: https://www.tiktok.com/@xlaeta | XLAETA\n----------------------------------------------------------------- \nKamera: Canon 80D \nVideoschnitt: Adobe Premiere Pro CC \nMusik: http://audionetwork.com \n\nBusiness Mail: info@xlaeta.de \nImpressum: http://xlaeta.de/impressum"
        },
        {
            "channelID": "UCQF_9Vhw22JHPfp3tveRS8g",
            "postID": "0JKhJR198ms",
            "content": "https://i.ytimg.com/vi/0JKhJR198ms/maxresdefault.jpg",
            "title": "Get Ready with me SILVESTER // Laura Sophie",
            "description": "Ich hoffe, ihr hattet eine schöne Feier oder was ihr gemacht habt xD \nFür fragen, Anregungen oder Wünsche bin ich in den Kommentaren aktiv. \n\nBis zum nächsten Video, danke für deine Unterstützung ❤️\n\nHier noch andere Informationen :\nMein Equipment :\n\nKamera: Canon Eos 700 D\nLaptop: Mac book pro retina 15\nSchneideprogramm: final cut pro X\nSoftboxen von Amazon\n\n-\n\nFolgt mir auf :\n\ninstagram: Laurasophie\nmusically: laurasophxe\nsnapchat: laurasmomentx\nask: itslaurasophie"
        },
        {
            "channelID": "UCkhcCPued3JClKU_HPEvApA",
            "postID": "0qvMzSNcBm0",
            "content": "https://i.ytimg.com/vi/0qvMzSNcBm0/maxresdefault.jpg",
            "title": "So nutzt du den Dyson Airwrap \"richtig\" \uD83E\uDD2A - Viral Trend Check! (Vlog)",
            "description": "Haarliebe Shootings, ein krasser Zimtschnecken-Tipp und ich überprüfe, was dieser virale Dyson-Hack wirklich kann. \uD83D\uDE05 Let's gooo!\nKostenlos meinen Kanal Abonnieren: http://bit.ly/DominoKatiAbo\n\nInstagram @DominoKati\nFacebook.com/DominoKati\nTwitter @DominoKati\nSpotify @DominoKati\n\nMeine Kamera*: http://amzn.to/2n724KF\nMein Objektiv*: http://amzn.to/2n73XHm\nMein Mikrofon* :http://amzn.to/2lKKb4a\nVerlängerungskabel*: http://amzn.to/2lKRoRI\nMikro Stativ*: http://amzn.to/2lKDGhP\n\n*Dieser Link ist bei dem Amazon Affiliate Partnerprogramm mit meinem Profil hinterlegt. Falls das Produkt über diesen Link gekauft werden sollte, werde ich zu einem kleinen Teil an dem Gewinn beteiligt. Es ist allerdings nicht zwingend notwendig es über den folgenden Link zu erwerben.\n\nMusic by:\nwww.audionetwork.com\nwww.epidemicsound.com\n\nGeschäftliche Anfragen: info@dominokati.de"
        },
        {
            "channelID": "UC6dLuMKBIj906bKTsgP3a5Q",
            "postID": "0RzQ4umu6qE",
            "content": "https://i.ytimg.com/vi/0RzQ4umu6qE/maxresdefault.jpg",
            "title": "Meine Lieblings Parfüms",
            "description": "Besten Parfüms für Männer\n#parfüm #duft #fragrance\n\nUnsere Parfum-Brand: https://fragrance.one \n\n10 Parfums für Männer:\nhttp://amzn.to/2aVGQtr\n\nMein Hauptkanal: \nhttps://www.youtube.com/jeremyfragrance\n\nFacebook:\nhttps://www.facebook.com/JeremyFragrance\n\nInstagram: \nhttps://instagram.com/jeremyfragrance/\n\nWeb: \nhttp://www.jeremyfragrance.com\n\nJeremy Fragrance ist teil des Amazon Associates Programm. Über die oben stehenden Links wird Umsatz generiert und nach messbaren Erfolg eine Provision gezahlt."
        },
        {
            "channelID": "UCA6Kbx7VknlpK5ktHFwTfGA",
            "postID": "19uHWAR8Z_0",
            "content": "https://i.ytimg.com/vi/19uHWAR8Z_0/maxresdefault.jpg",
            "title": "MAKE-UP mit meinen HIGH END FAVORITEN ✨ DOUGLAS MAKEUP LOOK I Cindy Jane",
            "description": "Ihr habt es euch gewünscht und hier ist es: Ein Full Face Make-Up Look mit meinen High End Favoriten von Douglas :) KOSTENLOS abonnieren und Glocke aktivieren, um keine Videos zu verpassen ♥\nDir gefallen meine Video? Zeige es mir gerne \uD83D\uDC4D\uD83C\uDFFC \uD83D\uDCDD! \n\n► FOLGE MIR AUF INSTAGRAM: \n      https://www.instagram.com/cindyjane/\n\n►FOLGE MIR AUF YOUTUBE: \n     http://www.youtube.com/subscription_center?add_user=puffelpumm\n♡  NEUE VIDEOS immer MITTWOCH & SONNTAG\n\n►BUSINESS E-MAIL: \n     Cindy.Jane@outlook.de\n\n● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● \n\n► IM VIDEO ERWÄHNT: \n Primer: https://www.stylink.it/ojQQFnmm5\nCorrector: https://www.stylink.it/6Zppsd66J\nFoundation: https://www.stylink.it/QPrrszLLg\nPuder: https://www.stylink.it/V5ZZFJAAn\nBronzer: https://www.stylink.it/94AASdw2m\nBlush: https://www.stylink.it/bnQQSVZm0\nHighlighter: https://www.stylink.it/XlZZCkAjO\nAugenbrauenstift: https://www.stylink.it/2bQQuqv2X\nAugenbrauengel: https://www.stylink.it/dpQQu4O5P\nLidschatten: https://www.stylink.it/5YyyF7nN4\nFixing Spray: https://www.stylink.it/ZQyyI9LYa\nKajal: https://www.stylink.it/mnQQS7dGp\nMascara: https://www.stylink.it/j4AASe5nv\nLippenstift: https://www.stylink.it/vaJJfm5jn\n\n\n►Gefragte PLAYLISTS:\nMeine Favoriten: https://bit.ly/2MZLIPf\nLive Tests: https://bit.ly/2Qo5hTx\nVlogs: https://bit.ly/37GVAFI\nHauls: https://bit.ly/2SQNTIN\nMeine Haare: https://bit.ly/2QoFwm6\nMake-Up Tutorials: https://bit.ly/37BHsxb\nHochzeit: https://bit.ly/2MXynHo\nRoutinen: https://bit.ly/2FjjaMm\n\n● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● ● \n\n►VIDEO-EQUIPMENT:\nKamera: https://amzn.st/1RYS6V1P7Y\nObjektv: https://amzn.st/OIC2DWV2WM\nMikrofon: https://amzn.st/IINY5X19J7\nFernauslöser: https://amzn.st/I7M6MVP8EQ\nBeleuchtung: https://amzn.st/XII8HU2SI2\nVlog-Kamera: https://amzn.st/ENC9NDAE3X\nMikrofon: https://amzn.st/JSMS7C51QW\n\n►MUSIK: https://www.epidemicsound.com/\n\nHinweis: \nDiese Infobox enthält Affiliate Links (Amazon EU Partnerprogramm. Stylink & ZANOX). Wenn Du über einen dieser Links ein Produkt kaufst, unterstützt du mich. Für dich bleibt der Preis unverändert."
        },
        {
            "channelID": "UCGWSzxsb4UXsQf_FRQ7rR1g",
            "postID": "1P8TtOlG5Ik",
            "content": "https://i.ytimg.com/vi/1P8TtOlG5Ik/maxresdefault.jpg",
            "title": "ICH SCHMINKE MARVYN MACNIFICENT | Joey's Jungle",
            "description": "Ich habe den Make-Up-Profi Marvyn Macnificent geschminkt. Und zwar GANZ ALLEINE. Leider hat man das dem Ergebnis am Ende auch angesehen \uD83D\uDE05\n\nMARVYN'S VIDEO: https://youtu.be/uZi5M7UQvUU\n\nWürde mich sehr über einen Daumen nach oben freuen, wenn euch das Video gefallen hat! :)\n----------------------------------------------------------------------------------------------------------------\n\nSOCIAL MEDIA LINKS:\n► Instagram: http://goo.gl/xwoblg (joeys_jungle)\n► Twitter: http://goo.gl/EbeSKF\n► Snapchat: joeys_jungle\n► Facebook: http://goo.gl/Ri7zOn\n----------------------------------------------------------------------------------------------------------------\n► Letztes Video: https://youtu.be/wLa9PoBqs_E\n► Abonnieren: https://goo.gl/ROiSh3\n► Bloopers-Playlist: https://goo.gl/zY78wo\n----------------------------------------------------------------------------------------------------------------\nBusiness Contact: \n► For Inquiries, please email: business@joeysjungle.de\n----------------------------------------------------------------------------------------------------------------\nWenn ihr das lest, dann schreibt \"Flodder Party Deluxe!\" in die Kommentare :D\n----------\n1.276.279"
        },
    ]

    const post = posts[2];

    return (
        <MainContent title={platform} subTitle={categories.find(c => c.key == preSelectedCategory)?.name} onBack={() => {
            setGoBack(true);
        }}>

            <Row justify="start" gutter={12} >
                <Col span={12}>
                    <YoutubePost postId={post.postID} title={post.title} description={post.description} thumbnail={post.content} ></YoutubePost>
                </Col>
                <Col span={12}>
                    <Row gutter={[0, 25]}>
                        <Col span={24}>
                            <CategorySelector onSelect={setSelectedCategories}>
                            </CategorySelector>
                        </Col>
                        <Col span={24}>
                            <Row justify="start" gutter={25}>
                                <Col>
                                    <Button type="default" size="large">Skip</Button>
                                </Col>
                                <Col>
                                    <Button type="primary" size="large">Bestätigen</Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </MainContent>
    )
}

export default ClassificationDetail;