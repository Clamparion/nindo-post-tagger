import { Card } from "antd";
import Meta from "antd/lib/card/Meta";
import { FC } from "react";

type YoutubePostProps = {
    postId: string
    title: string,
    description: string,
    thumbnail?: string
}

const YoutubePost: FC<YoutubePostProps> = ({ title, description, thumbnail, postId }) => {
    return (
        <Card
           
            hoverable
            cover={<img alt="thumbnail" src={thumbnail ?? ''} />}
            extra={postId ? <a href={`https://www.youtube.com/watch?v=${postId}`} target="_blank">Original anzeigen</a> : undefined}
        >
            <Meta title={title} description={description} />
        </Card>
    )
}

export default YoutubePost;


