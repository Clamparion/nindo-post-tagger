import { FC } from "react";
import MainContent from "../../components/MainContent";
import { Tabs } from 'antd';
import ClassificationOverviewTable from "./ClassificationOverviewTable";
import { SocialMediaPlatform } from "../types";
const { TabPane } = Tabs;

const ClassificationOverview: FC = () => {

    const platforms = [
        SocialMediaPlatform.YouTube,
        SocialMediaPlatform.Instagram,
        SocialMediaPlatform.TikTok
    ]

    return (
        <MainContent title="Klassifizierungen">
            <Tabs defaultActiveKey="1" size="large" type="card">
                {platforms.map(platform =>
                    <TabPane tab={platform} key={platform}>
                        <ClassificationOverviewTable platform={platform}></ClassificationOverviewTable>
                    </TabPane>
                )
                }
            </Tabs>
        </MainContent>
    )
}

export default ClassificationOverview;