import { FC } from "react";
import { Table} from 'antd';
import { SocialMediaPlatform } from "../types";
import { Link } from "react-router-dom";
import { Category, useCategories } from "../categories";


type ClassificationOverviewTableProps  = {
    platform: SocialMediaPlatform
}

const ClassificationOverviewTable: FC<ClassificationOverviewTableProps> = ({ platform }) => {
    const categories = useCategories();

    const columns = [
        {
            title: `Kategorie (${platform})`,
            dataIndex: 'category',
            key: 'category',
            render: (text: string, record: any) => (
                <Link to={`/classify/${platform}/${record.categoryKey}`}>{text}</Link>
            ),
        },
        {
            title: 'Anzahl getaggter Datensätze',
            dataIndex: 'taggedCount',
            key: 'taggedCount',
            sorter: {
                compare: (a: any, b: any) => a.taggedCount - b.taggedCount,
                multiple: 1,
            },
        }
    ];

    const data = categories.map((c: Category) => {
        return {
            key: c.id,
            category: c.name,
            categoryKey: c.key,
            taggedCount: c.name.length * platform.length * 574
        }
    })

    return (
        <Table pagination={false} size="large" columns={columns} dataSource={data} />
    )
}

export default ClassificationOverviewTable;