import { FC } from 'react';
import { Menu, Layout, Button } from 'antd';
import './App.css';
import Login from './auth/Login';
import { BrowserRouter as Router, Route, Link, Redirect } from "react-router-dom";
import PrivateRoute from './components/PrivateRoute';
import ClassificationOverview from './classification/classification-overview/ClassificationOverview';
import Logout from './auth/Logout';
import ClassificationDetail from './classification/classification-detail/ClassificationDetail';

const { Header, Content } = Layout;

const App: FC = () => (
  <Router>
    <Layout className="layout">
      <Header className="header">
        <Menu theme="dark" mode="horizontal">
          <Link to="/logout"><Button>Logout</Button></Link>
        </Menu>
      </Header>
      <Content className="content" >
        <Route path="/login" component={Login} />
        <Route path="/logout" component={Logout} />
        <PrivateRoute path="/classify/:platform/:preSelectedCategory">
          <ClassificationDetail></ClassificationDetail>
        </PrivateRoute>
        <PrivateRoute path='/classify'>
          <ClassificationOverview></ClassificationOverview>
        </PrivateRoute>
        <Route path="/">
          <Redirect to="/classify" />
        </Route>
      </Content>
    </Layout>
  </Router>)


export default App;
