import { Spin } from 'antd';
import { useState } from 'react';
import { FC } from 'react';
import { Redirect } from 'react-router';
import { fakeAuth } from './auth';

const Logout: FC = () => {
    const [
        redirectToReferrer,
        setRedirectToReferrer
    ] = useState(false)

    fakeAuth.signout(() => {
        setRedirectToReferrer(true)
    })


    if (redirectToReferrer === true) {
        return <Redirect to='/classify' />
    }

    return <Spin />
};

export default Logout;