import axios from "axios"

import React from "react"
import { appConfig } from "../config";

export const fakeAuth = {
    isAuthenticated: false,
    authenticate(username: string, password: string, cb: Function, reject: Function) {
        this.isAuthenticated = true
        const config = appConfig();
        axios.post( `${config.apiUrl}/auth/login`, { username, password }).then(res => {
            window.localStorage.setItem('token', res.data);
            cb();
        }).catch(err => {
            reject(err)
        })
    },
    signout(cb: Function) {
        this.isAuthenticated = false
        setTimeout(cb, 100) // fake async
    }
}