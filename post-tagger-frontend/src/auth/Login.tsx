import { Form, Input, Button, Alert } from 'antd';
import { useState } from 'react';
import { FC } from 'react';
import { Redirect } from 'react-router';
import { fakeAuth } from './auth';
import './Login.css';
const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};
const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
};

const Login: FC = () => {
    const [
        redirectToReferrer,
        setRedirectToReferrer
    ] = useState(false)

    const [hasError, setHasError] = useState(false);

    const [isLoading, setIsLoading] = useState(false);

    const login = (username: string, password: string) => {
        setIsLoading(true);
        fakeAuth.authenticate(username, password, () => {
            setRedirectToReferrer(true)
            setIsLoading(false);
        }, (err: any) => {
            setHasError(err);
            setIsLoading(false);
        })
    }

    const onFinish = (values: any) => {
        console.log('Success:', values);
        login(values.username, values.password);
    };

    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };

    if (redirectToReferrer === true) {
        return <Redirect to='/classify' />
    }

    return (
        <div className="login-form">
            <Form
                {...layout}
                name="basic"
                initialValues={{ remember: true }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
            >
                {
                    hasError ? <Alert
                        style={{marginBottom: '25px'}}
                        message="Error"
                        description="Username or password incorrect!"
                        type="error"
                        showIcon
                    /> : <></>
                }
                <Form.Item
                    label="Username"
                    name="username"
                    rules={[{ required: true, message: 'Please input your username!' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="password"
                    rules={[{ required: true, message: 'Please input your password!' }]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit" loading={isLoading}>
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </div>

    );
};

export default Login;