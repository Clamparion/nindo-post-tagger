import { FC } from "react"
import { Redirect, Route } from "react-router-dom"
import { fakeAuth } from "../auth/auth"

type PrivateRouteProps = {
    path: string,
}

const PrivateRoute: FC<PrivateRouteProps> = ({ path, children }) => {
    return (
        <Route exact path={path} render={({ location }) => {
            return fakeAuth.isAuthenticated === true
                ? children
                : <Redirect to={{
                    pathname: '/login',
                    state: { from: location }
                }}
                />
        }} />
    )
}
export default PrivateRoute;