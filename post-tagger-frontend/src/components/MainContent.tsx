import { PageHeader } from "antd";
import { FC } from "react";
import './MainContent.css';

type MainContentProps = {
    title: string,
    subTitle?: string,
    onBack?: (e?: React.MouseEvent<HTMLDivElement>) => void
}

const MainContent: FC<MainContentProps> = ({ title, subTitle, onBack, children }) => {
    return (
        <>
            <div className="layout-main">
                <PageHeader
                    title={title}
                    subTitle={subTitle ?? ''}
                    onBack={onBack}
                />
                <div className="content-main">
                    {children}
                </div>
            </div>
        </>
    )
}

export default MainContent;